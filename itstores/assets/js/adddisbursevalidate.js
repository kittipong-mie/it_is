/**
 * Created by MIE on 9/3/2559.
 */
$(document).ready(function () {

    $('#add-form').validate({
        rules: {
            type_equipment: {
                required: true
            },
            equipment: {
                required: true
            },
            amount: {
                required: true,
                number: true
            },
            dh_name: {
                required: true
            },
            office: {
                required: true
            },
            dh_lender_name: {
                required: true
            }

        },
        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element.text('OK!').addClass('valid')
                .closest('.control-group').removeClass('error').addClass('success');
        }
    });

});
