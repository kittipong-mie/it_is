/**
 * Created by MIE on 5/3/2559.
 */
$(document).ready(function () {
    $("#type_equipment").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "disburse_history/get_equipment/" + id,
            type: "GET",
            success: function (html) {
                $("#equipment").html(html);
            }
        });

    });
});

$(document).ready(function () {
    $("#equipment").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "disburse_history/get_equipment_amount_unit/" + id,
            type: "GET",
            success: function (html) {
                $("#amount_unit").html(html);
            }
        });

    });
});