<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <base href="<?php echo site_url(); ?>">
    <!-- Bootstrap core CSS -->
    <link href="assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/page/user_list.css" rel="stylesheet">

</head>
<body>
<div class="container">
    <div class="col-xs-10">
    <div class="row">
        <h1>จัดการข้อมูล การเติมวัสดุคอมพิวเตอร์</h1>
    </div>

    <div class="row">
        <p>
            <a href="add_history/add" class="btn btn-primary"> เพิ่มข้อมูล </a>
        </p>
    </div>

    <div class="row">

        <!-- <div id="body">
        <?php /*alert($record); */ ?>
    </div>-->
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>รหัส</th>
                <th>ชื่อวัสดุคอมพิวเตอร์</th>
                <th>จำนวน</th>
                <th>หน่วย</th>
                <th>ราคา</th>
                <th>พนักงาน</th>
                <th>เวลาเพิ่มข้อมูล</th>
                <th>วันที่จัดซื้อ</th>
                <th>การจัดการข้อมูล</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($record as $row) { ?>

                <tr>
                    <td><?php echo($row['ah_id']); ?></td>
                    <td><?php echo($row['eq_name']); ?></td>
                    <td><?php echo($row['eq_amount']); ?></td>
                    <td><?php echo($row['eq_unit']); ?></td>
                    <td><?php echo($row['ah_price']); ?></td>
                    <td><?php echo($row['u_fl']); ?></td>
                    <td><?php echo($row['ah_time']); ?></td>
                    <td><?php echo($row['ah_purchasing_time']); ?></td>
                    <td><a href="add_history/delete/<?php echo($row['ah_id']); ?>" class="btn btn-danger">ลบ</a></td>

                </tr>

            <?php } ?>
            </tbody>

        </table>
    </div>
</div>
</div>


</body>
</html>