<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title; ?></title>
    <base href="<?php echo site_url(); ?>">
    <link href="assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/page/user_list.css" rel="stylesheet">

</head>
<body>

<div class="container">
    <div class="row">
        <h1>เพิ่มข้อมูล การเติมวัสดุคอมพิวเตอร์ </h1>
    </div>
    <div class="row">
        <form action="Add_history/save" method="post">

            <div class="form-group">
                <div class="col-md-8">
                <label for="equipment">วัสดุคอมพิวเตอร์:</label>
                <select class="form-control" name="equipment" id="equipment">
                    <option>Lan cable</option>
                    <option>rj-45</option>
                    <option>cover rj-45</option>
                    <option>HDD-1TB</option>
                    <option>HDD-2TB</option>
                    <option>Keyboard USB</option>
                    <option>Mouse USB</option>
                    <option>DVD-R</option>
                    <option>CD_R</option>
                </select>
            </div>


            <div class="form-group">
                <div class="col-md-8"><label for="amount">จำนวน:</label> <input type="text" name="amount" maxlength="50" size="50" class="form-control" id="amount"
                                              placeholder="จำนวน"></div>
                <div class="col-md-4"><br><label for="unit"> หน่วย </label></div>


            </div>
<br>
            <div class="form-group">
                <div class="col-md-8">  <label for="price">ราคา:</label>
                    <input type="text" name="price" class="form-control" id="pwd" placeholder="ราคา"></div>

            </div>

            <div class="form-group">
                <div class="col-md-8">
                <label for="ah_purchasing_time">วันที่จัดซื้อ:</label>
                <input type="date" name="ah_purchasing_time" class="form-control" id="ah_purchasing_time"
                       placeholder="วันที่จัดซื้อ">
                    <br>
            </div>
                <div class="col-md-8">
                <div class="form-actions">
                <button type="submit" class="btn btn-success">เพิ่ม</button>
                <a class="btn btn-default " href="add_history">กลับ</a>
            </div>
        </form>
    </div>
</div>


</body>
</html>