<div class="container">
    <div class="row">
        <ul class="breadcrumb">
            <li><a href="welcome"><span class="glyphicon glyphicon-home" aria-hidden="true"> หน้าแรก</a></li>
            <li><a href="user"> รายการข้อมูลผู้ใช้</a></li>
            <li class="active">เพิ่มข้อมูลผู้ใช้</li>
        </ul>
        <h1>เพิ่มข้อมูลผู้ใช้ </h1>
    </div>
    <div class="row">
        <form class="form-horizontal" action="user/save" method="post">
            <div class="row">
                <div class="form-group">
                    <label class="col-xs-2 control-label" for="Name">ชื่อ-นามสกุล:</label>

                    <div class="col-xs-5">
                        <input type="text" name="name" maxlength="50" size="50" class="form-control" id="name"
                               required="required" placeholder="กรุณากรอก ชื่อ-นามสกุล">
                    </div>
                    <h5>*</h5>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label class="col-xs-2 control-label" for="email">Email:</label>

                    <div class="col-xs-5">

                        <input type="email" name="email" maxlength="50" size="50" class="form-control" id="email "
                               required="required" placeholder="กรุณากรอก email">
                    </div>
                    <h5>*</h5>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label class="col-xs-2 control-label" for="pwd">รหัสผ่าน:</label>

                    <div class="col-xs-5">
                        <input type="password" name="password" class="form-control" id="pwd"
                               required="required"  placeholder="กรุณากรอก รหัสผ่าน"></div>
                    <h5>*</h5>
                </div>

            </div>

            <div class="row">

                <div class="form-group">
                    <label class="col-xs-2 control-label" for="role">สิทธิ์:</label>

                    <div class="col-xs-5">
                        <select class="form-control" name="role" id="role">
                            <option value="1">Admin</option>
                            <option value="2" selected>Staff</option>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="form-group">
                    <label class="col-xs-2 control-label" for="status">สถานะ:</label>

                    <div class="col-xs-5">
                        <select class="form-control" name="status" id="status">
                            <option value="1">ใช้งาน</option>
                            <option value="0" selected>ไม่ใช้งาน</option>
                        </select>
                    </div>

                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success" onclick="return confirm('กรุณายืนยัน การบันทึก ?')"
                        >เพิ่ม</button>

                    <a class="btn btn-default " href="user">กลับ</a>


                </div>
            </div>

        </form>

    </div>
</div>