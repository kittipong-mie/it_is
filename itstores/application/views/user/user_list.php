<div class="container">


    <div class="row">
        <ul class="breadcrumb">
            <li><a href="welcome"><span class="glyphicon glyphicon-home" aria-hidden="true"> หน้าแรก </a></li>
            <li class="active">รายการข้อมูลผู้ใช้</li>
        </ul>
        <h1>จัดการข้อมูลผู้ใช้งาน</h1>
    </div>

    <div class="row">
        <p>
            <a href="user/add" class="btn btn-primary"> เพิ่มข้อมูล </a>
        </p>
    </div>

    <div class="row">

        <!-- <div id="body">
        <?php /*alert($record); */ ?>
    </div>-->
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>รหัส</th>
                <th>ชื่อ</th>
                <th>อีเมล</th>
                <th>สิทธิ์</th>
                <th>สถานะ</th>
                <th>การจัดการข้อมูล</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($results as $data) { ?>

                <tr>
                    <td><?php echo($data->u_id); ?></td>
                    <td><?php echo($data->u_name); ?></td>
                    <td><?php echo($data->u_email); ?></td>
                    <td><?php if ($data->u_role == '1'): ?>
                            <h4>admin</h4>
                        <?php elseif ($data->u_role == '2'): ?>
                            <h4>staff</h4>
                        <?php endif; ?>
                    </td>
                    <td><?php if ($data->u_status == '1'): ?>
                            <h4><span class="label label-success">ใช้งาน</span></h4>
                        <?php elseif ($data->u_status == '0'): ?>
                            <h4><span class="label label-default">ไม่ใช้งาน</span></h4>
                        <?php endif; ?>
                    </td>
                    <td><a href="user/edit/<?php echo($data->u_id); ?>" class="btn btn-success">แก้ไข</a>&nbsp;&nbsp;
                        <a href="user/delete/<?php echo($data->u_id); ?>" class="btn btn-danger"
                           onclick="return confirm('กรุณายืนยัน การลบข้อมูล ?')">ลบ</a></td>

                </tr>

            <?php } ?>

            </tbody>

        </table>
        <p><?php echo $links; ?></p>
    </div>

