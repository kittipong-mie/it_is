<div class="container">
    <div class="row">
        <ul class="breadcrumb">
            <li><a href="welcome"><span class="glyphicon glyphicon-home" aria-hidden="true"> หน้าแรก</a></li>
            <li><a href="user"> รายการข้อมูลผู้ใช้</a></li>
            <li class="active">แก้ไขข้อมูลผู้ใช้</li>
        </ul>
        <h1>แก้ไข ข้อมูลผู้ใช้ </h1>
    </div>


    <div class="row">
        <form class="form-horizontal" action="user/save" method="post">
            <?php foreach ($result as $data) { ?>
                <input type="hidden" name="u_id" value="<?php echo($data->u_id); ?>" />
                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="Name">ชื่อ-นามสกุล:</label>

                        <div class="col-xs-5">
                            <input type="text" value="<?php echo($data->u_name); ?>" name="name" maxlength="50" size="50"
                                   class="form-control" id="email " required="required"
                                   placeholder="กรุณากรอก ชื่อ-นามสกุล">
                        </div>
                        <h5>*</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="email">Email:</label>

                        <div class="col-xs-5">

                            <input type="email" value="<?php echo($data->u_email); ?>" name="email" maxlength="50" size="50"
                                   class="form-control" id="email "
                                   required="required" placeholder="กรุณากรอก email">
                        </div>
                        <h5>*</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="pwd">รหัสผ่าน:</label>

                        <div class="col-xs-5">
                            <input type="text" value="<?php echo($data->u_pass); ?>" name="password" class="form-control"
                                   id="pwd"
                                   required="required" placeholder="กรุณากรอก รหัสผ่าน"></div>
                        <h5>*</h5>
                    </div>

                </div>

                <div class="row">

                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="role">สิทธิ์:</label>

                        <div class="col-xs-5">
                            <select class="form-control" name="role" id="role">
                                <?php if ($data->u_role == '1'): ?>
                                    <option value="1" selected>Admin</option>
                                    <option value="2">Staff</option>
                                <?php elseif ($data->u_role == '2'): ?>
                                    <option value="1">Admin</option>
                                    <option value="2" selected>Staff</option>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="status">สถานะ:</label>

                        <div class="col-xs-5">
                            <select class="form-control" name="status" id="status">
                                <?php if ($data->u_status == '1'): ?>
                                    <option value="1" selected>ใช้งาน</option>
                                    <option value="0">ไม่ใช้งาน</option>
                                <?php elseif ($data->u_status == '0'): ?>
                                    <option value="1">ใช้งาน</option>
                                    <option value="0" selected>ไม่ใช้งาน</option>
                                <?php endif; ?>

                            </select>
                        </div>

                    </div>
                </div>
            <?php } ?>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success" onclick="return confirm('กรุณายืนยัน การบันทึก ?')"
                        >บันทึก
                    </button>

                    <a class="btn btn-default " href="user">กลับ</a>


                </div>
            </div>

        </form>

    </div>
</div>