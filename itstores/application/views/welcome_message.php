<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <base href="<?php echo site_url(); ?>">
    <!-- Bootstrap core CSS -->
    <link href="assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/page/user_list.css" rel="stylesheet">

</head>
<body>

<?php echo $head;?>

<br>
<br>


<div class="row">
    <div class="col-md-3">
        <?php echo $sidebar;?>
    </div>



    <div class="col-md-9">
        <?php echo $content;?>

    </div>
</div>

<?php echo $footer;?>
</body>
</html>
