<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <base href="<?php echo site_url(); ?>">
    <!-- Bootstrap core CSS -->
    <link href="../../assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../assets/css/page/login.css" rel="stylesheet">
</head>

<body>

<div class="container">
     <form class="form-signin" action="login/validate_credentials" method="post">
        <img src="../../assets/images/logo_dpt.gif" class="img-circle" alt="Cinque Terre" width="100" height="100">
        <h2 class="form-signin-heading">กรุณาลงชื่อเข้าระบบ</h2>
        <label for="email" class="sr-only">Email address</label>
        <input type="email" name="email" id="email" class="form-control" placeholder="กรุณากรอก Email address" required
               autofocus>
        <label for="password" class="sr-only">Password</label>
        <input type="password" name="password" id="password" class="form-control" placeholder="กรุณากรอก Password" required>

        <button class="btn btn-lg btn-primary btn-block" type="submit">เข้าสู่ระบบ &nbsp; &nbsp; <span
                class="glyphicon glyphicon-log-in"></span></button>
        <!--  <button class="btn btn-lg btn-primary btn-block" type="reset">cancel  <span class="glyphicon glyphicon-log-in"></span> </button>-->
    </form>

</div>
<!-- /container -->


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../assets/js/bootstrap.min.js"></script>
</body>
</html>
