<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
    <title>Page Title</title>
    <link href="../../assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../assets/css/page/user_list.css" rel="stylesheet">
</head>
<body>
<div id="container">
    <h1>User</h1>
    <div id="body">
        <?php

        foreach($results as $data) {
            echo $data->u_name . " - " .$data->u_email . " - " .$data->u_status . " - " . $data->u_role . "<br>";
        }
        ?>
        <p><?php echo $links; ?></p>
    </div>
    <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</div>
</body>
</html>