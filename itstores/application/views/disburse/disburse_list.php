<div class="container">
    <div class="row">
        <ul class="breadcrumb">
            <li><a href="welcome"><span class="glyphicon glyphicon-home" aria-hidden="true"> หน้าแรก </a></li>
            <li class="active">รายการข้อมูลการเบิกวัสดุคอมพิวเตอร์</li>
        </ul>
        <h1>จัดการข้อมูลการเบิกวัสดุคอมพิวเตอร์</h1>
    </div>

    <div class="row">
        <p>
            <a href="disburse_history/add" class="btn btn-primary"> เพิ่มข้อมูล </a>
        </p>
    </div>

    <div class="row">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>รหัส</th>
                <th>ชื่อวัสดุคอมพิวเตอร์</th>
                <th>จำนวน</th>
                <th>หน่วย</th>
                <th>ชื่อผู้เบิก</th>
                <th>หน่วยงาน</th>
                <th>พนักงานจ่ายวัสดุ</th>
                <th>พนักงานบันทึกข้อมูล</th>
                <th>วันที่เบิก</th>
                <th>การจัดการข้อมูล</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($results as $data) { ?>
                <tr>
                    <td><?php echo($data->dh_id); ?></td>
                    <td><?php echo($data->eq_name); ?></td>
                    <td><?php echo($data->eq_amount); ?></td>
                    <td><?php echo($data->eq_unit); ?></td>
                    <td><?php echo($data->dh_name); ?></td>
                    <td><?php echo($data->off_name); ?></td>
                    <td><?php echo($data->dh_lender_name); ?></td>
                    <td><?php echo($data->u_name); ?></td>
                    <td><?php echo($data->dh_time); ?></td>
                    <td><a href="disburse_history/delete/<?php echo($data->dh_id); ?>"
                           class="btn btn-danger"  onclick="return confirm('กรุณายืนยัน การลบข้อมูล ?')">ลบ</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <p><?php echo $links; ?></p>
    </div>
</div>