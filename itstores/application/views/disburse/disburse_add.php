<!--<link href="assets/css/page/bootstrap-combined.min.css" rel="stylesheet">-->

<div class="container">

    <div class="row">
        <ul class="breadcrumb">
            <li><a href="welcome"><span class="glyphicon glyphicon-home" aria-hidden="true"> หน้าแรก </a></li>
            <li><a href="disburse_history"> รายการข้อมูลการเบิกวัสดุคอมพิวเตอร์</a></li>
            <li class="active">เพิ่มข้อมูลการเบิกวัสดุคอมพิวเตอร์</li>
        </ul>
        <h1>เพิ่มข้อมูล การเบิกวัสดุคอมพิวเตอร์ </h1>
    </div>
    <div class="form-group">
        <form class="form-horizontal" action="disburse_history/save" method="post" id="add-form">

            <?php $u_id = $this->session->userdata('u_id'); ?>
            <div class="row">
                <input type="hidden" name="u_id" value="<?php echo $u_id; ?>"/>

                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="type_equipment">ประเภทวัสดุ:</label>

                        <div class="col-xs-5">
                            <select class="form-control" name="type_equipment" id="type_equipment">
                                <option selected="selected" value="">-- กรุณาเลือก ประเภทวัสดุคอมพิวเตอร์ --</option>
                                <?php foreach ($results as $data) { ?>
                                    <option
                                        value="<?php echo $data['type_id']; ?>"><?php echo $data['type_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="equipment">วัสดุคอมพิวเตอร์:</label>

                        <div class="col-xs-5">

                            <select class="form-control" name="equipment" id="equipment">
                                <option selected="selected" value="">-- กรุณาเลือก วัสดุคอมพิวเตอร์ --</option>
                            </select>

                        </div>

                        <label for="amount_unit" id="amount_unit"></label>


                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="amount">จำนวน:</label>

                        <div class="col-xs-2">
                            <input type="number" name="amount" maxlength="50"
                                   size="30" class="form-control" id="amount"
                                   placeholder="กรุณากรอกจำนวน">
                        </div>
                        <h5>*</h5>

                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="dh_name">ชื่อผู้เบิก:</label>

                        <div class="col-xs-5">
                            <input type="text" name="dh_name" maxlength="50"
                                   size="50" class="form-control" id="dh_name"
                                   placeholder="กรุณากรอกชื่อผู้เบิก">
                        </div>
                       
                        <h5>*</h5>

                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="office">หน่วยงาน:</label>

                        <div class="col-xs-5">

                            <select class="form-control" name="office" id="office">
                                <option selected="selected" value="">-- กรุณาเลือก หน่วยงาน --</option>
                                <?php foreach ($results2 as $data) { ?>
                                    <option
                                        value="<?php echo $data['off_id']; ?>"><?php echo $data['off_name']; ?></option>
                                <?php } ?>
                            </select>

                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="dh_lender_name">พนักงานจ่ายวัสดุ:</label>

                        <div class="col-xs-5">
                            <input type="text" name="dh_lender_name"
                                   maxlength="50" size="50" class="form-control"
                                   id="dh_lender_name"
                                   placeholder="กรุณากรอกชื่อพนักงานจ่ายวัสดุ">
                        </div>

                        <h5>*</h5>
                    </div>
                </div>
                <br>

                <div class="form-action">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success"
                                onclick="return confirm('กรุณายืนยัน การบันทึก ?')"
                            >เพิ่ม
                        </button>
                        <a class="btn btn-default " href="disburse_history">กลับ</a>
                    </div>
                </div>

        </form>
    </div>
</div>
</div>

<script src="assets/js/ajax.js"></script>
<script src="assets/js/adddisbursevalidate.js"></script>
<script src="assets/js/additional-methods.js"></script>
