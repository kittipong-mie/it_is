<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <base href="<?php echo site_url(); ?>">
    <!-- Bootstrap core CSS -->
    <link href="assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/page/user_list.css" rel="stylesheet">

</head>
<body>

<div class="container">

    <div class="row">
        <h1>testblog</h1>
    </div>

    <div class="row">
        <p>
         <h1>testblog</h1>
        </p>
    </div>

    <div class="row">

        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>title</th>
                <th>content</th>
                <th>date</th>
            </tr>
            </thead>

            <tbody>

            <?php foreach ($record as $row) { ?>
                <tr>
                    <td><?php echo($row['id']); ?></td>
                    <td><?php echo($row['title']); ?></td>
                    <td><?php echo($row['content']); ?></td>
                    <td><?php echo($row['date']); ?></td>
                </tr>
            <?php } ?>
            </tbody>

        </table>
    </div>
</div>


</body>
</html>
