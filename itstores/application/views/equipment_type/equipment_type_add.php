<div class="container">
    <div class="row">
        <ul class="breadcrumb">
            <li><a href="welcome"><span class="glyphicon glyphicon-home" aria-hidden="true"> หน้าแรก</a></li>
            <li><a href="equipment_type"> รายการข้อมูลประเภทวัสดุ</a></li>
            <li class="active">เพิ่มข้อมูลประเภทวัสดุ</li>
        </ul>
        <h1>เพิ่มข้อมูลประเภทวัสดุ </h1>
    </div>
    <div class="row">
        <form class="form-horizontal" action="equipment_type/save" method="post">

            <div class="row">
                <div class="form-group">
                    <label class="col-xs-2 control-label" for="Name">ชื่อประเภทวัสดุ:</label>

                    <div class="col-xs-5">
                        <input type="text" name="type_name" maxlength="50" size="50" class="form-control" id="type_name"
                               required="required" placeholder="กรุณากรอก ชื่อประเภทวัสดุ">
                    </div>
                    <h5>*</h5>
                </div>
            </div>
            <br>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success" onclick="return confirm('กรุณายืนยัน การบันทึก ?')"
                        >เพิ่ม
                    </button>
                    <a class="btn btn-default " href="equipment_type">กลับ</a>
                </div>
            </div>

        </form>
    </div>
</div>