<div class="container">
    <div class="col-xs-10">
        <div class="row">
            <ul class="breadcrumb">
                <li><a href="welcome"><span class="glyphicon glyphicon-home" aria-hidden="true"> หน้าแรก</a></li>
                <li class="active">รายการข้อมูลประเภทวัสดุ</li>
            </ul>
            <h1>จัดการข้อมูล ประเภทวัสดุ</h1>
        </div>

        <div class="row">
            <p>
                <a href="equipment_type/add" class="btn btn-primary"> เพิ่มข้อมูล </a>
            </p>
        </div>
        <!-- <div id="body">
        <?php /*alert($record); */ ?>
    </div>-->
        <div class="row">
            <table class="table table-striped table-bordered table-hover" width="100" border="1">
                <thead>
                <tr>
                    <th>รหัส</th>
                    <th>ชื่อประเภท</th>
                    <th>การจัดการข้อมูล</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($results as $data) { ?>

                    <tr>
                        <td><?php echo($data->type_id); ?></td>
                        <td><?php echo($data->type_name); ?></td>
                        <td><a href="equipment_type/edit/<?php echo($data->type_id); ?>" class="btn btn-success">แก้ไข</a>&nbsp;&nbsp;
                            <a href="equipment_type/delete/<?php echo($data->type_id); ?>" class="btn btn-danger"
                               onclick="return confirm('กรุณายืนยัน การลบข้อมูล ?')">ลบ</a></td>
                    </tr>

                <?php } ?>
                </tbody>

            </table>
            <p><?php echo $links; ?></p>
        </div>
    </div>

</div>