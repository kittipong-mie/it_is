<div class="container">
    <div class="row">
        <ul class="breadcrumb">
            <li><a href="welcome"><span class="glyphicon glyphicon-home" aria-hidden="true"> หน้าแรก</a></li>
            <li><a href="equipment_type"> รายการข้อมูลประเภทวัสดุ</a></li>
            <li class="active">แก้ไขข้อมูลประเภทวัสดุ</li>
        </ul>
        <h1>แก้ไขข้อมูลประเภทวัสดุ </h1>
    </div>
    <div class="row">
        <form action="equipment_type/save" method="post">
            <?php foreach ($result as $data) { ?>
                <input type="hidden" name="type_id" value="<?php echo($data->type_id); ?>" />
                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="Name">ชื่อประเภทวัสดุ:</label>

                        <div class="col-xs-5">
                            <input type="text" value="<?php echo($data->type_name); ?>" name="type_name" maxlength="50" size="50" class="form-control" id="type_name"
                                   required="required" placeholder="กรุณากรอก ชื่อประเภทวัสดุ">
                        </div>
                        <h5>*</h5>
                    </div>
                </div>
                <br>
            <?php } ?>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success" onclick="return confirm('กรุณายืนยัน การบันทึก ?')"
                        >บันทึก
                    </button>
                    <a class="btn btn-default " href="equipment_type">กลับ</a>
                </div>
            </div>
    </div>
</div>