
    <?php $u_role = $this->session->userdata('u_role');?>
    <?php $u_status = $this->session->userdata('u_status');?>
    <?php if( ($u_role == '1') and ($u_status == '1')): ?>

        <ul class="nav nav-pills nav-stacked">
            <li <?php if($menu_name=="home"){?> class="active" <?php }?>><a href="welcome"><span class="glyphicon glyphicon-home" aria-hidden="true"> หน้าแรก</span></a></li>
            <li <?php if($menu_name=="add_history"){?> class="active" <?php }?>><a href="add_history">ข้อมูลการเติมวัสดุ</a></li>
            <li <?php if($menu_name=="disburse_history"){?> class="active" <?php }?>><a href="disburse_history">ข้อมูลการเบิกวัสดุ</a></li>
            <li <?php if($menu_name=="equipment"){?> class="active" <?php }?>><a href="equipment">ข้อมูลวัสดุคอมพิวเตอร์</a></li>
            <li <?php if($menu_name=="office"){?> class="active" <?php }?>><a href="office">ข้อมูลหน่วยงาน</a></li>
            <li <?php if($menu_name=="equipment_type"){?> class="active" <?php }?>><a href="equipment_type">ข้อมูลประเภทวัสดุ</a></li>
            <li <?php if($menu_name=="user"){?> class="active" <?php }?>><a href="user">ข้อมูลผู้ใช้งาน</a></li>
        </ul>

    <?php elseif ( ($u_role == '2') and ($u_status == '1')): ?>

        <ul class="nav nav-pills nav-stacked">
            <li <?php if($menu_name=="home"){?> class="active" <?php }?>><a href="welcome"><span class="glyphicon glyphicon-home" aria-hidden="true"> หน้าแรก</span></a></li>
            <li <?php if($menu_name=="add_history"){?> class="active" <?php }?>><a href="add_history">ข้อมูลการเติมวัสดุ</a></li>
            <li <?php if($menu_name=="disburse_history"){?> class="active" <?php }?>><a href="disburse_history">ข้อมูลการเบิกวัสดุ</a></li>
        </ul>

    <?php else: ?>

        <h3>ผู้ใช้ไม่มีสถานะใช้งานได้ กรุณาติดต่อผู้ดูและระบบ</h3>

    <?php endif; ?>