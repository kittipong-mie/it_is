<div class="container">
    <!--    <nav class="navbar navbar-default navbar-fixed-top">-->
    <!--        <div class="navbar-header">-->
    <!--            <a class="navbar-brand" href="#">ระบบบริหารวัสดุคอมพิวเตอร์</a>-->
    <!--            <ul class="nav navbar-nav navbar-right">-->
    <!--                <li><a href="#"><span class="glyphicon glyphicon-user"></span>Username</a></li>-->
    <!--                <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>-->
    <!--            </ul>-->
    <!--        </div>-->
    <!--    </nav>-->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="welcome">ระบบบริหารวัสดุคอมพิวเตอร์</a>
            </div>
           <?php $username = $this->session->userdata('username');?>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="welcome"><span class="glyphicon glyphicon-user"></span> <?php echo $username;?></a></li>
                <li><a href="login/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
            </ul>

        </div>

    </nav>
</div>

