<div class="container">
    <div class="row">
        <ul class="breadcrumb">
            <li><a href="welcome"><span class="glyphicon glyphicon-home" aria-hidden="true"> หน้าแรก </a></li>
            <li class="active">รายการข้อมูลหน่วยงาน</li>
        </ul>
        <h1>จัดการข้อมูล หน่วยงาน</h1>
    </div>

    <div class="row">
        <p>
            <a href="office/add" class="btn btn-primary"> เพิ่มข้อมูล </a>
        </p>
    </div>
    <!-- <div id="body">
        <?php /*alert($record); */ ?>
    </div>-->
    <div class="row">
        <table class="table table-striped table-bordered table-hover" width="200" border="1">
            <thead>
            <tr>
                <th>รหัส</th>
                <th>ชื่อหน่วยงาน</th>
                <th>สถานะ</th>
                <th>การจัดการข้อมูล</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($results as $data) { ?>

                <tr>
                    <td><?php echo($data->off_id); ?></td>
                    <td><?php echo($data->off_name); ?></td>
                    <td><?php if ($data->off_status == '1'): ?>
                            <h4><span class="label label-success">ใช้งาน</span></h4>
                        <?php elseif ($data->off_status == '0'): ?>
                            <h4><span class="label label-default">ไม่ใช้งาน</span></h4>
                        <?php endif; ?></td>
                    <td><a href="office/edit/<?php echo($data->off_id); ?>" class="btn btn-success">แก้ไข</a>&nbsp;&nbsp;
                        <a href="office/delete/<?php echo($data->off_id); ?>" class="btn btn-danger"
                           onclick="return confirm('กรุณายืนยัน การลบข้อมูล ?')">ลบ</a></td>

                </tr>

            <?php } ?>
            </tbody>

        </table>
        <p><?php echo $links; ?></p>
    </div>

</div>