<div class="container">
    <div class="row">
        <ul class="breadcrumb">
            <li><a href="welcome"><span class="glyphicon glyphicon-home" aria-hidden="true"> หน้าแรก</a></li>
            <li><a href="office"> รายการข้อมูลหน่วยงาน</a></li>
            <li class="active">เพิ่มหน่วยงาน</li>
        </ul>
        <h1>เพิ่มชื่อหน่วยงาน</h1>
    </div>
    <div class="row">
        <form class="form-horizontal" action="office/save" method="post">
            <div class="row">
                <div class="form-group">
                    <label class="col-xs-2 control-label" for="Name">ชื่อหน่วยงาน:</label>

                    <div class="col-xs-5">
                        <input type="text" name="off_name" maxlength="50" size="50" class="form-control" id="off_name"
                               required="required" placeholder="กรุณากรอกชื่อหน่วยงาน">
                    </div>
                    <h5>*</h5>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label class="col-xs-2 control-label" for="Name">สถานะ:</label>

                    <div class="col-xs-5">
                        <select class="form-control" name="status" id="status">
                            <option  value="1" selected>ใช้งาน</option>
                            <option  value="0">ไม่ใช้งาน</option>
                        </select>
                    </div>

                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success" onclick="return confirm('กรุณายืนยัน การบันทึก ?')"
                        >เพิ่ม
                    </button>
                    <a class="btn btn-default " href="office">กลับ</a>
                </div>
            </div>
        </form>
    </div>
</div>