<div class="container">
    <div class="row">
        <ul class="breadcrumb">
            <li><a href="welcome"><span class="glyphicon glyphicon-home" aria-hidden="true"> หน้าแรก</a></li>
            <li><a href="office"> รายการข้อมูลหน่วยงาน</a></li>
            <li class="active">แก้ไขข้อมูลหน่วยงาน</li>
        </ul>
        <h1>แก้ไขข้อมูลหน่วยงาน </h1>
    </div>
    <div class="row">
        <form class="form-horizontal" action="office/save" method="post">
            <?php foreach ($result as $data) { ?>
                <input type="hidden" name="off_id" value="<?php echo($data->off_id); ?>" />
                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="Name">ชื่อหน่วยงาน:</label>

                        <div class="col-xs-5">
                            <input type="text" value="<?php echo($data->off_name); ?>" name="off_name" maxlength="50" size="50" class="form-control" id="off_name"
                                   required="required" placeholder="กรุณากรอกชื่อหน่วยงาน">
                        </div>
                        <h5>*</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="Name">สถานะ:</label>

                        <div class="col-xs-5">
                            <select class="form-control" name="status" id="status">
                                <?php if ($data->off_status == '1'): ?>
                                    <option value="1" selected>ใช้งาน</option>
                                    <option value="0">ไม่ใช้งาน</option>
                                <?php elseif ($data->off_status == '0'): ?>
                                    <option value="1">ใช้งาน</option>
                                    <option value="0" selected>ไม่ใช้งาน</option>
                                <?php endif; ?>

                            </select>
                        </div>

                    </div>
                </div>
                <br>
            <?php } ?>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success" onclick="return confirm('กรุณายืนยัน การบันทึก ?')"
                        >เพิ่ม
                    </button>
                    <a class="btn btn-default " href="office">กลับ</a>
                </div>
            </div>
        </form>
    </div>
</div>