<div class="container">
    <div class="row">
        <ul class="breadcrumb">
            <li><a href="welcome"><span class="glyphicon glyphicon-home" aria-hidden="true"> หน้าแรก </a></li>
            <li class="active">รายการข้อมูลวัสดุคอมพิวเตอร์</li>
        </ul>
        <h1>จัดการข้อมูลวัสดุคอมพิวเตอร์</h1>
    </div>

    <div class="row">
        <p>
            <a href="equipment/add" class="btn btn-primary"> เพิ่มข้อมูล </a>
        </p>
    </div>

    <div class="row">

        <!-- <div id="body">
        <?php /*alert($record); */ ?>
    </div>-->
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>รหัส</th>
                <th>ประเภทวัสดุ</th>
                <th>ชื่อวัสดุคอมพิวเตอร์</th>
                <th>จำนวน</th>
                <th>หน่วย</th>
                <th>การจัดการข้อมูล</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($results as $data) { ?>

                <tr>
                    <td><?php echo($data->eq_id); ?></td>
                    <td><?php echo($data->type_name); ?></td>
                    <td><?php echo($data->eq_name); ?></td>
                    <td><?php echo($data->eq_amount); ?></td>
                    <td><?php echo($data->eq_unit); ?></td>
                    <td><a href="equipment/edit/<?php echo($data->eq_id); ?>" class="btn btn-success">แก้ไข</a>&nbsp;&nbsp;
                        <a href="equipment/delete/<?php echo($data->eq_id); ?>" class="btn btn-danger"  onclick="return confirm('กรุณายืนยัน การลบข้อมูล ?')">ลบ</a></td>

                </tr>

            <?php } ?>
            </tbody>

        </table>
        <p><?php echo $links; ?></p>
    </div>
</div>
