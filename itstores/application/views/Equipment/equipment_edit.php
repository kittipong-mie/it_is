<div class="container">
    <div class="row">
        <ul class="breadcrumb">
            <li><a href="welcome"><span class="glyphicon glyphicon-home" aria-hidden="true"> หน้าแรก</a></li>
            <li><a href="equipment"> รายการข้อมูลวัสดุคอมพิวเตอร์</a></li>
            <li class="active">แก้ไขข้อมูลวัสดุคอมพิวเตอร์</li>
        </ul>
        <h1>แก้ไขข้อมูลวัสดุคอมพิวเตอร์ </h1>
    </div>
    <div class="row">
        <form action="equipment/save" method="post">

            <?php foreach ($result as $datas) { ?>

                <?php $type =$datas['type_id']; ?>
                <input type="hidden" name="eq_id" value="<?php echo$datas['eq_id']; ?>" />

                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="type_equipment">ประเภทวัสดุ:</label>

                        <div class="col-xs-5">

                            <select class="form-control" name="type_equipment">
                                <?php foreach ($results as $data) { ?>
                                    <option value="<?php echo $data['type_id']; ?>"<?= $type == $data['type_id'] ? ' selected="selected"' : ''; ?>><?php echo $data['type_name']; ?></option>
                                <?php } ?>
                            </select>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="Name">ชื่อวัสดุคอมพิวเตอร์:</label>

                        <div class="col-xs-5">
                            <input type="text" value=" <?php echo $datas['eq_name'];?>" name="name" maxlength="50"
                                   size="50" class="form-control" id="name"
                                   required="required" placeholder="กรุณากรอกชื่อวัสดุ">
                        </div>
                        <h5>*</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="amount">จำนวน:</label>

                        <div class="col-xs-5">
                            <input type="number" value="<?php echo  $datas['eq_amount']; ?>" name="amount" maxlength="50"
                                   size="30" class="form-control" id="amount"
                                   required="required" placeholder="กรุณากรอกจำนวน">
                        </div>
                        <h5>*</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="unit">หน่วยนับ:</label>

                        <div class="col-xs-5">
                            <input type="text" value=" <?php echo  $datas['eq_unit']; ?>" name="unit" maxlength="50"
                                   size="50" class="form-control" id="unit"
                                   required="required" placeholder="กรุณากรอกชื่อวัสดุ">
                        </div>
                        <h5>*</h5>
                    </div>
                </div>
                <br>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success"
                                onclick="return confirm('กรุณายืนยัน การบันทึก ?')"
                            >บันทึก
                        </button>
                        <a class="btn btn-default " href="equipment">กลับ</a>
                    </div>
                </div>
            <?php } ?>
        </form>
    </div>
</div>