<html>
<head>
    <title>My Form</title>
</head>
<meta charset="utf-8">


<!-- Bootstrap core CSS -->
<link href="../../assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="../../assets/css/page/user_list.css" rel="stylesheet">
<script src="../../assets/js/jquery-1.12.1.js"></script>
<body>

<?php echo form_open('form'); ?>
<div class="col-xs-5">
<h4>Username</h4>
<input class="form-control" type="text" name="username" value="<?php echo set_value('username'); ?>" size="50" />
<?php echo form_error('username', '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>

', '</div>'); ?>
<h4>Password</h4>
<input type="text" name="password" value="<?php echo set_value('password'); ?>" size="50" />

<h4>Password Confirm</h4>
<input type="text" name="passconf" value="<?php echo set_value('passconf'); ?>" size="50" />

<h4>Email Address</h4>
<input type="text" name="email" value="<?php echo set_value('email'); ?>" size="50" />

<div><input type="submit" value="Submit" /></div>

</form>
<?php echo validation_errors(); ?>
    </div>
</body>
</html>
