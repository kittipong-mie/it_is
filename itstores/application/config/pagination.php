<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//$pagination["base_url"] = base_url() . "user/index";
//$pagination["total_rows"] = $this->user_model->record_count();
$pagination["per_page"] = 10;
$pagination["uri_segment"] = 3;
//pagination bootstrap pagination
$pagination['full_tag_open'] = '<ul class="pagination pagination-sm">';
$pagination['full_tag_close'] = '</ul>';
$pagination['num_tag_open'] = '<li>';
$pagination['num_tag_close'] = '</li>';
$pagination['cur_tag_open'] = '<li class="active"><span>';
$pagination['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
$pagination['prev_tag_open'] = '<li>';
$pagination['prev_tag_close'] = '</li>';
$pagination['next_tag_open'] = '<li>';
$pagination['next_tag_close'] = '</li>';
$pagination['first_link'] = '&laquo;';
$pagination['prev_link'] = '&lsaquo;';
$pagination['last_link'] = '&raquo;';
$pagination['next_link'] = '&rsaquo;';
$pagination['first_tag_open'] = '<li>';
$pagination['first_tag_close'] = '</li>';
$pagination['last_tag_open'] = '<li>';
$pagination['last_tag_close'] = '</li>';

$config['pagination']	= $pagination;