<?php
/**
 * Created by IntelliJ IDEA.
 * User: vanpersie
 * Date: 24/2/2559
 * Time: 14:05
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class office_model extends CI_Model
{
    private $table = "office";

    function __construct()
    {
        parent::__construct();
    }


    function get_office_list()
    {
        $query = $this->db->get_where('office', array('off_status' => '1'));
        return $query->result_array();
    }

    function insert_office($data)
    {
        $this->db->insert('office', $data);
        $result = $this->db->affected_rows();
        return $result;
    }

    public function record_count_office()
    {
        return $this->db->count_all("office");
    }

    function  update_office($data, $id)
    {
        $this->db->where('off_id', $id);
        $this->db->update($this->table, $data);
        $result = $this->db->affected_rows();
        return $result;
    }

    function  delete_office($id)
    {
        $this->db->where('off_id', $id);
        $this->db->delete($this->table);
        $result = $this->db->affected_rows();
        return $result;

    }

    public function fetch_office($limit, $start)
    {
        $this->db->limit($limit, $start);
        $query = $this->db->get("office");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function get_office($off_id)
    {
        $query = $this->db->get_where('office', array('off_id' => $off_id));
        $data = $query->result();
        return $data;
    }


}
