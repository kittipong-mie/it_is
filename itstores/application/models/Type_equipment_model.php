<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Created by IntelliJ IDEA.
 * User: vanpersie
 * Date: 11/2/2559
 * Time: 14:25
 */
class Type_equipment_model extends CI_Model
{
    private $table = "type_equipment";

    function __construct()
    {
        parent::__construct();
    }
    function get_type_dropdown_list()
    {
        $this->db->from('type_equipment');
        $this->db->order_by('type_id');
        $result = $this->db->get();
        $return = array();
        if($result->num_rows() > 0) {
            foreach($result->result_array() as $row) {
                $return[$row['type_id']] = $row['type_name'];
            }
        }
alert($return);
        return $return;

    }

    function get_type_equipment_list()
    {
        $query = $this->db->get('type_equipment');
        return  $query->result_array();

    }

    function insert_type_equipment($data)
    {
        $this->db->insert('type_equipment', $data);
        $result = $this->db->affected_rows();
        return $result;
    }

    public function record_count_type()
    {
        return $this->db->count_all("type_equipment");
    }

    function  update_type_equipment($data, $id)
    {
        $this->db->where('type_id', $id);
        $this->db->update($this->table, $data);
        $result = $this->db->affected_rows();
        return $result;
    }

    function  delete_type_equipment($id)
    {
        $this->db->where('type_id', $id);
        $this->db->delete($this->table);
        $result = $this->db->affected_rows();
        return $result;

    }

    public function fetch_type_equipment($limit, $start)
    {
        $this->db->limit($limit, $start);
        $query = $this->db->get("type_equipment");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function get_type_equipment($type_id)
    {
        $query = $this->db->get_where('type_equipment', array('type_id' => $type_id));
        $data = $query->result();
        return $data;
    }


}
