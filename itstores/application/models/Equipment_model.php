<?php
/**
 * Created by IntelliJ IDEA.
 * User: vanpersie
 * Date: 24/2/2559
 * Time: 21:15
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Equipment_model extends CI_Model
{
    private $table = "equipment";

    function __construct()
    {
        parent::__construct();
    }


    function get_equipment_list()
    {
        $query = $this->db->get('equipment');
        return $query->result_array();
    }

    function insert_equipment($data)
    {
        $this->db->insert('equipment', $data);
        $result = $this->db->affected_rows();
        return $result;
    }

    public function record_count_equipment()
    {
        return $this->db->count_all("equipment");
    }

    function  update_equipment($data, $id)
    {
        $this->db->where('eq_id', $id);
        $this->db->update($this->table, $data);
        $result = $this->db->affected_rows();
        return $result;
    }

    function  delete_equipment($id)
    {
        $this->db->where('eq_id', $id);
        $this->db->delete($this->table);
        $result = $this->db->affected_rows();
        return $result;

    }

    function result_getall()
    {

        $this->db->select('*');
        $this->db->from('equipment');
        $this->db->join('type_equipment', 'equipment.type_id = type_equipment.type_id', 'left');
        $query = $this->db->get();
        return $query->result();

    }

    public function fetch_equipment($limit, $start)
    {
        $this->db->limit($limit, $start);
        $this->db->from('equipment');
        $this->db->join('type_equipment', 'equipment.type_id = type_equipment.type_id', 'left');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function get_equipment($id)
    {
        $query = $this->db->get_where('equipment', array('eq_id' => $id));
        $data = $query->result_array();
        return $data;
    }

    function  update_equipment_amount($datas, $id)
    {
        $this->db->where('eq_id', $id);
        $this->db->update('equipment', $datas);
        $results = $this->db->affected_rows();
        return $results;
    }

}
