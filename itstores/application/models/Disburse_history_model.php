<?php
/**
 * Created by IntelliJ IDEA.
 * User: MIE
 * Date: 1/3/2559
 * Time: 14:21
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Disburse_history_model extends CI_Model
{
    private $table = "disburse_history";

    function __construct()
    {
        parent::__construct();
    }


    function get_disburse_history_list()
    {
        $query = $this->db->get('disburse_history');
        return $query->result_array();
    }

    function insert_disburse_history($data)
    {
        $this->db->insert('disburse_history', $data);
        $result = $this->db->affected_rows();
        return $result;
    }

    public function record_count_disburse_history()
    {
        return $this->db->count_all("disburse_history");
    }


    function  delete_disburse_history($id)
    {
        $this->db->where('dh_id', $id);
        $this->db->delete($this->table);
        $result = $this->db->affected_rows();
        return $result;

    }

    public function fetch_disburse_history($limit, $start)
    {
        $this->db->limit($limit, $start);
        $this->db->select('disburse_history.*,equipment.eq_name,equipment.eq_unit,office.*,user.*');
        $this->db->from('disburse_history');
        $this->db->join('equipment', 'disburse_history.eq_id = equipment.eq_id', 'left');
        $this->db->join('office', 'disburse_history.off_id = office.off_id', 'left');
        $this->db->join('user', 'disburse_history.u_id = user.u_id', 'left');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            //  alert($data);
            return $data;
        }
        return false;
    }

    function get_disburse_history($dh_id)
    {
        $query = $this->db->get_where('disburse_history', array('dh_id' => $dh_id));
        $data = $query->result();
        return $data;
    }


    function get_type()
    {
        $query = $this->db->query("SELECT type_name,type_id FROM type_equipment");

        if ($query->num_rows > 0) {
            $data = $query->result();
            return $data;
        }
    }


    function get_equipment_from_type($type_id)
    {
        $query = $this->db->query("SELECT eq_id,eq_name FROM equipment WHERE type_id = '{$type_id}'");
        $data = $query->result_array();
        return $data;
    }

    function get_equipment($eq_id)
    {
        $query = $this->db->query("SELECT eq_amount,eq_unit FROM equipment WHERE eq_id = '{$eq_id}'");
        $data = $query->result_array();
        return $data;
    }

    function get_disburse($id)
    {
        $query = $this->db->query("SELECT eq_id,eq_amount FROM disburse_history WHERE dh_id = '{$id}'");
         $data = $query->result_array();
             return $data;
    }


}
