<?php


class blogmodel extends CI_Model
{

    var $title = '';
    var $content = '';
    var $date = '';

    public function programming()
    {
        $aProgramming = array("PHP", "Ruby", "JAVA", "Python", "SQL");
        return $aProgramming;
    }

    function __construct()
    {
// Call the Model constructor
        parent::__construct();
    }

    function get_last_ten_entries()
    {
        $query = $this->db->get('entries', 10);
        return $query->result_array();
    }

    function insert_entry()
    {
        $this->title = $_POST['title']; // please read the below note
        $this->content = $_POST['content'];
        $this->date = time();

        $this->db->insert('entries', $this);
    }

    function update_entry()
    {
        $this->title = $_POST['title'];
        $this->content = $_POST['content'];
        $this->date = time();

        $this->db->update('entries', $this, array('id' => $_POST['id']));
    }

}