<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Created by IntelliJ IDEA.
 * User: vanpersie
 * Date: 11/2/2559
 * Time: 14:25
 */
class User_model extends CI_Model
{
    private $table = "user";

    function __construct()
    {
        parent::__construct();
    }


    function get_user_list()
    {
        $query = $this->db->get('user');
        return $query->result_array();
    }

    function insert_user($data)
    {
        $this->db->insert('user', $data);
        $result = $this->db->affected_rows();
        return $result;
    }

    public function record_count()
    {
        return $this->db->count_all("user");
    }

    function  update_user($data, $id)
    {
        $this->db->where('u_id', $id);
        $this->db->update($this->table, $data);
        $result = $this->db->affected_rows();
        return $result;
    }

    function  delete_user($id)
    {
        $this->db->where('u_id', $id);
        $this->db->delete($this->table);
        $result = $this->db->affected_rows();
        return $result;

    }

    public function fetch_user($limit, $start)
    {
        $this->db->limit($limit, $start);
        $query = $this->db->get("user");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function get_user($u_id)
    {
        $query = $this->db->get_where('user', array('u_id' => $u_id));
        $data = $query->result();
        return $data;
    }
    function validate($email, $password)
    {
        $this -> db -> select('*');
        $this -> db -> from('user');
        $this -> db -> where('u_email', $email);
        $this -> db -> where('u_pass', $password);
        $this -> db -> limit(1);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

}
