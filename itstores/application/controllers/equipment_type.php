<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class equipment_type extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (($this->session->userdata('logged_in'))&&(( ($this->session->userdata('u_role') == '1') && ($this->session->userdata('u_status') == '1')))) {
            //alert($this->session->userdata('logged_in'));
            $this->load->helper("url");
            $this->load->model("type_equipment_model");
            $this->load->database();
            $this->load->library("pagination");
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }

    public function index()
    {

//pagination set

        $config = array();
        $config["base_url"] = base_url() . "equipment_type/index";
        $config["total_rows"] = $this->type_equipment_model->record_count_type();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        //config bootstrap pagination
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><span>';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = '&laquo;';
        $config['prev_link'] = '&lsaquo;';
        $config['last_link'] = '&raquo;';
        $config['next_link'] = '&rsaquo;';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

//-------------------------------------------------------//
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data = array(
            "title" => $this->config->item('page_title'),
            "name" => $this->config->item('name'),
            "results" => $this->type_equipment_model->fetch_type_equipment($config["per_page"], $page),
            "links" => $this->pagination->create_links()
        );

//--------------------------------------------------------//

        /*load data list */
        $content = $this->load->view('equipment_type/equipment_type_list', $data, true);
        /*โหลด layout */
        $head = $this->load->view('layout/head', array(), true);
        $menu = array(
            "menu_name" => "equipment_type"
        );
        $sidebar = $this->load->view('layout/sidebar', $menu, true);
        $footer = $this->load->view('layout/footer', array(), true);
        $data = array(
            "head" => $head,
            "sidebar" => $sidebar,
            "footer" => $footer,
            "content" => $content
        );
        $this->load->view('layout/main', $data);

    }

    public function  add()
    {
        $data = array(
            "title" => $this->config->item('page_title'),
        );
        $content =  $this->load->view('equipment_type/equipment_type_add', $data,true);
        /*โหลด layout */


        $head = $this->load->view('layout/head', array(), true);
        $menu = array(
            "menu_name" => "user"
        );
        $sidebar = $this->load->view('layout/sidebar', $menu, true);
        $footer = $this->load->view('layout/footer', array(), true);
        $data = array(
            "head" => $head,
            "sidebar" => $sidebar,
            "footer" => $footer,
            "content" => $content
        );
        $this->load->view('layout/main', $data);
    }

    public function  edit($type_id = NULL)
    {
        if ($type_id == NULL) {
            $type_id = $this->uri->segment(3);
        }
        // Load view
        // Get type info by ID


        $data = array(
            "title" => $this->config->item('page_title'),
            "result" => $data['query'] = $this->type_equipment_model->get_type_equipment($type_id)
        );
        //  alert($data);

        $content =  $this->load->view('equipment_type/equipment_type_edit', $data,true);
        /*โหลด layout */


        $head = $this->load->view('layout/head', array(), true);
        $menu = array(
            "menu_name" => "user"
        );
        $sidebar = $this->load->view('layout/sidebar', $menu, true);
        $footer = $this->load->view('layout/footer', array(), true);
        $data = array(
            "head" => $head,
            "sidebar" => $sidebar,
            "footer" => $footer,
            "content" => $content
        );
        $this->load->view('layout/main', $data);
    }


    public function  delete($type_id = null)
    {
        if ($type_id == NULL) {
            $type_id = $this->uri->segment(3);
        }

        $id = $type_id;
        //เรียกใช้ฟังก์ชั่น ใน Model
        $result = $this->type_equipment_model->delete_type_equipment($id);
        if ($result == 1) {
            echo '<script>alert("ลบข้อมูลเรียบร้อยแล้ว!");</script>';
            redirect('equipment_type', 'refresh');
        } else {
            $this->session->set_flashdata("message", "Record Not Updated!");
            redirect('equipment_type', 'refresh');
        }
        redirect('equipment_type', 'refresh');
    }


    public function save()
    {
        //โหลด Model
        if ($this->input->post('type_id') == null) {
            $data = array(
                'type_name' => $this->input->post('type_name'),

            );

            //เรียกใช้ฟังก์ชั่น ใน Model
            $result = $this->type_equipment_model->insert_type_equipment($data);
            if ($result == 1) {
                echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว!");</script>';
                redirect('equipment_type', 'refresh');
            } else {
                $this->session->set_flashdata("message", "Record Not Updated!");
                redirect('equipment_type', 'refresh');
            }

        } else {
            $id = $this->input->post('type_id');
            $data = array(
                'type_name' => $this->input->post('type_name')

            );

            //เรียกใช้ฟังก์ชั่น ใน Model
            $result = $this->type_equipment_model->update_type_equipment($data, $id);
            if ($result == 1) {
                echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว!");</script>';
                redirect('equipment_type', 'refresh');
            } else {
                $this->session->set_flashdata("message", "Record Not Updated!");
                redirect('equipment_type', 'refresh');
            }
            redirect('equipment_type', 'refresh');
        }

    }


}
