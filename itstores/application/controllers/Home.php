<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>test
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		function __construct()
		{
			parent::__construct();
			if (($this->session->userdata('logged_in'))&&(( ($this->session->userdata('u_role') == '1') && ($this->session->userdata('u_status') == '1')))) {
				//alert($this->session->userdata('logged_in'));
				$data=array(
					"title"=>$this->config->item('page_title'),
					"name"=>$this->config->item('name')
				);

				$this->load->view('home/content',$data);
			} else {
				//If no session, redirect to login page
				redirect('login', 'refresh');
			}

		}

	}

}
