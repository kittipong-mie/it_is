<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

    }

    public function index()
    {

        //โหลด Model
        $this->load->model('blogmodel');
        //เรียกใช้ฟังก์ชั่น ใน Model
        $record = $this->blogmodel->get_last_ten_entries();
        //data=ผลลัพท์ที่เป็น array
        $data = array("record" => $record);
        //ส่งค่าให้ data หน้า view blog
        $this->load->view('blog', $data);

        // var_dump($data);//**แสดงผลข้อมูล

        //  $news = $this->blogmodel->programming();
        //  var_dump($news);//**แสดงผลข้อมูล
    }

}
