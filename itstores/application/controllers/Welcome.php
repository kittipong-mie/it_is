<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (($this->session->userdata('logged_in'))&&(( (($this->session->userdata('u_role') == '1') OR ($this->session->userdata('u_role') == '2'))&& ($this->session->userdata('u_status') == '1')))) {
            //alert($this->session->userdata('logged_in'));

        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    public function index()
    {

        $data = array(
            "title" => $this->config->item('page_title'),
            "name" => $this->config->item('name')
        );
        /*load content  */
        $content = $this->load->view('home/content', $data, true);
        /*โหลด layout */


        $head = $this->load->view('layout/head', array(), true);
        $menu = array(
            "menu_name" => "home"
        );
        $sidebar = $this->load->view('layout/sidebar', $menu, true);
        $footer = $this->load->view('layout/footer', array(), true);
        $data = array(
            "head" => $head,
            "sidebar" => $sidebar,
            "footer" => $footer,
            "content" => $content
        );
        $this->load->view('layout/main', $data);

    }


}
