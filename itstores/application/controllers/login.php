<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
      // alert($this->session->userdata);
        //   $this->session->set_userdata('id',200);
        // $this->session->unset_userdata('id');
        //   alert($this->session->userdata);
        //   alert($this->session->userdata('id'));
        if ($this->session->userdata('is_logged_in')) {
            redirect('welcome');
        } else {
            $data = array(
                "title" => $this->config->item('page_title'),
                "name" => $this->config->item('name')
            );
            $this->load->view('login', $data);
        }

    }

    function validate_credentials()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        // alert($email);
        // alert($password);
        $result = $this->user_model->validate($email, $password);
        // alert($result);


        if ($result > 0) {
            foreach ($result as $row) {
                $u_id = $row['u_id'];
                $name = $row['u_name'];
                $u_role = $row['u_role'];
                $u_status = $row['u_status'];

            }

            $this->session->set_userdata('logged_in', true);
            $this->session->set_userdata('u_id',$u_id);
            $this->session->set_userdata('username', $name);
            $this->session->set_userdata('u_role', $u_role);
            $this->session->set_userdata('u_status', $u_status);
            redirect('welcome');
        } else {

            echo "<script>
                            alert('ชื่ออีเมล,พาสเวิร์ดไม่ถูกต้อง');
                                window.location.href='index';
                  </script>";
        }
    }


    function logout()
    {
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('u_id');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('u_role');
        $this->session->unset_userdata('u_status');
        session_destroy();
        alert ($this->session->userdata);
        redirect('login');
    }

}



