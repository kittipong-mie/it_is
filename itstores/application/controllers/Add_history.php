<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_history extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        if (($this->session->userdata('logged_in'))and(( (($this->session->userdata('u_role') == '1') or ($this->session->userdata('u_role') == '2'))and ($this->session->userdata('u_status') == '1')))) {
            //alert($this->session->userdata('logged_in'));
            $this->load->helper("url");
            $this->load->model("equipment_model");
            $this->load->model("type_equipment_model");
            $this->load->model("office_model");
            $this->load->model("disburse_history_model");
            $this->load->database();
            $this->load->library("pagination");
            $this->disburse = new Disburse_history_model();
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }


    public function index()
    {




//                <th>ID</th>
//                <th>ชื่อวัสดุคอมพิวเตอร์</th>
//                <th>จำนวน</th>
//                <th>หน่วย</th>
//                <th>ราคา</th>
//                <th>ผู้เพิ่ม</th>
//                <th>เวลา</th>
//                <th>เวลาที่จัดซื้อ</th>
//                <th>การจัดการข้อมูล</th>
        $rs_add_story = array(
            array(
                "ah_id" => 1,
                "eq_name" => "การ์ดแลน PCI",
                "eq_amount" => 6,
                "eq_unit" => "ชิ้น",
                "ah_price" => "1000",
                "u_fl" => "สุรศัก พิชัยเชษ",
                "ah_time" => "2013-04-02 14:16:04",
                "ah_purchasing_time" => "2013-03-28"
            ),
            array(
                "ah_id" => 2,
                "eq_name" => "อุปกรณ์กระจายสัญญาณ (Switch) ชนิด 8 Port",
                "eq_amount" => 10,
                "eq_unit" => "กล่อง",
                "ah_price" => "500",
                "u_fl" => "จันทมณี พิชัยเชษ",
                "ah_time" => "2013-04-02 14:16:04",
                "ah_purchasing_time" => "2013-03-28"
            ),
            array(
                "ah_id" => 3,
                "eq_name" => "หัว RJ-45",
                "eq_amount" => 10,
                "eq_unit" => "กล่อง",
                "ah_price" => "2000",
                "u_fl" => "จารุทัศน์ มิ่งจินดา",
                "ah_time" => "2013-04-02 14:16:04",
                "ah_purchasing_time" => "2013-03-28"
            )


        );
        $record = array();
        foreach ($rs_add_story as $row) {

            /* if ($row['u_role']=='1') {
                 $u_role = "admin";
             }
             else{
                 $u_role = "staff";
             }*/
//            $row['u_role'] = $role[$row['u_role']];
//            $row['u_status'] = $status[$row['u_status']];

            array_push($record, $row);
        }

//        alert($record);
//                die;


        $data = array(
            "title" => $this->config->item('page_title'),
            "name" => $this->config->item('name'),
            "record" => $record
        );
    $content = $this->load->view('add_history/add_history_list', $data,true);
        /*โหลด เลย์เอ้า */
        $head = $this->load->view('layout/head', array(),true);
        $menu = array(
            "menu_name" => "add_history"
        );
        $sidebar = $this->load->view('layout/sidebar', $menu,true);
        $footer = $this->load->view('layout/footer', array(),true);
        $data = array(
            "head" => $head,
            "sidebar" => $sidebar,
            "footer" => $footer,
            "content" => $content
        );
        $this->load->view('layout/main', $data);

    }

    public function  add()
    {
        $data = array(
            "title" => $this->config->item('page_title'),
        );
        $this->load->view('add_history/add_history_add', $data);
    }

    public function  edit($ah_id)
    {
        alert($ah_id);
        alert($_POST);
        $data = array(
            "title" => $this->config->item('page_title'),
        );
        $this->load->view('add_history/add_history_edit', $data);
    }


    public function  delete($ah_id)
    {
        alert($ah_id);
    }


    public function save()
    {
        $v1 = $this->input->post('v1');
        $v2 = $this->input->post('v2');
        alert($_POST);
        alert($v1 + $v2);
        alert($v1 + $v2);

    }


}
