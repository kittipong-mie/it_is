<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Office extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (($this->session->userdata('logged_in'))&&(( ($this->session->userdata('u_role') == '1') && ($this->session->userdata('u_status') == '1')))) {
            //alert($this->session->userdata('logged_in'));
            $this->load->helper("url");
            $this->load->model("office_model");
            $this->load->database();
            $this->load->library("pagination");
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }

    public function index()
    {

        //pagination set

        $config = array();
        $config["base_url"] = base_url() . "office/index";
        $config["total_rows"] = $this->office_model->record_count_office();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        //config bootstrap pagination
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><span>';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = '&laquo;';
        $config['prev_link'] = '&lsaquo;';
        $config['last_link'] = '&raquo;';
        $config['next_link'] = '&rsaquo;';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

//-------------------------------------------------------//
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data = array(
            "title" => $this->config->item('page_title'),
            "name" => $this->config->item('name'),
            "results" => $this->office_model->fetch_office($config["per_page"], $page),
            "links" => $this->pagination->create_links()
        );

//--------------------------------------------------------//


        /*load data list */
        $content =  $this->load->view('office/office_list', $data,true);
        /*โหลด layout */
        $head = $this->load->view('layout/head', array(),true);
        $menu = array(
            "menu_name" => "office"
        );
        $sidebar = $this->load->view('layout/sidebar', $menu,true);
        $footer = $this->load->view('layout/footer', array(),true);
        $data = array(
            "head" => $head,
            "sidebar" => $sidebar,
            "footer" => $footer,
            "content" => $content
        );
        $this->load->view('layout/main', $data);

    }

    public function  add()
    {
        $data = array(
            "title" => $this->config->item('page_title'),
        );

        $content = $this->load->view('office/off_add', $data,true);
        /*โหลด layout */
        $head = $this->load->view('layout/head', array(),true);
        $menu = array(
            "menu_name" => "office"
        );
        $sidebar = $this->load->view('layout/sidebar', $menu,true);
        $footer = $this->load->view('layout/footer', array(),true);
        $data = array(
            "head" => $head,
            "sidebar" => $sidebar,
            "footer" => $footer,
            "content" => $content
        );
        $this->load->view('layout/main', $data);
    }

    public function  edit($off_id = null)
    {
        if ($off_id == NULL) {
            $off_id = $this->uri->segment(3);
        }
        // Load view
        // Get type info by ID


        $data = array(
            "title" => $this->config->item('page_title'),
            "result" => $data['query'] = $this->office_model->get_office($off_id)
        );
        //  alert($data);

        $content =   $this->load->view('office/off_edit', $data,true);
        /*โหลด layout */
        $head = $this->load->view('layout/head', array(),true);
        $menu = array(
            "menu_name" => "office"
        );
        $sidebar = $this->load->view('layout/sidebar', $menu,true);
        $footer = $this->load->view('layout/footer', array(),true);
        $data = array(
            "head" => $head,
            "sidebar" => $sidebar,
            "footer" => $footer,
            "content" => $content
        );
        $this->load->view('layout/main', $data);
    }



    public function  delete($off_id = null)
    {
        if ($off_id == NULL) {
            $off_id = $this->uri->segment(3);
        }

        $id = $off_id;
        //เรียกใช้ฟังก์ชั่น ใน Model
        $result = $this->office_model->delete_office($id);
        if ($result == 1) {
            echo '<script>alert("ลบข้อมูลเรียบร้อยแล้ว!");</script>';
            redirect('office', 'refresh');
        } else {
            $this->session->set_flashdata("message", "Record Not Updated!");
            redirect('office', 'refresh');
        }
        redirect('office', 'refresh');
    }



    public function save()
    {
        //โหลด Model
        if ($this->input->post('off_id') == null) {
            $data = array(
                'off_name' => $this->input->post('off_name'),
                'off_status' => $this->input->post('status')
            );

            //เรียกใช้ฟังก์ชั่น ใน Model
            $result = $this->office_model->insert_office($data);
            if ($result == 1) {
                echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว!");</script>';
                redirect('office', 'refresh');
            } else {
                $this->session->set_flashdata("message", "Record Not Updated!");
                redirect('office', 'refresh');
            }

        } else {
            $id = $this->input->post('off_id');
            $data = array(
                'off_name' => $this->input->post('off_name'),
                'off_status' => $this->input->post('status')

            );

            //เรียกใช้ฟังก์ชั่น ใน Model
            $result = $this->office_model->update_office($data, $id);
            if ($result == 1) {
                echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว!");</script>';
                redirect('office', 'refresh');
            } else {
                $this->session->set_flashdata("message", "Record Not Updated!");
                redirect('office', 'refresh');
            }
            redirect('office', 'refresh');
        }
    }


}
