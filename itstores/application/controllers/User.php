<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (($this->session->userdata('logged_in'))&&(( ($this->session->userdata('u_role') == '1') && ($this->session->userdata('u_status') == '1')))) {
            //alert($this->session->userdata('logged_in'));

        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    //ทดลองแบ่งหน้า
    public function index()
    {
        //pagination set
        $config = $this->config->item('pagination');
        $config["base_url"] = base_url() . "user/index";
        $config["total_rows"] = $this->user_model->record_count();
//-------------------------------------------------------//
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data = array(
            "title" => $this->config->item('page_title'),
            "name" => $this->config->item('name'),
            "results" => $this->user_model->fetch_user($config["per_page"], $page),
            "links" => $this->pagination->create_links()
        );
//--------------------------------------------------------//
        /*load data list */
        $content = $this->load->view('user/user_list', $data, true);
        /*โหลด layout */
        $head = $this->load->view('layout/head', array(), true);
        $menu = array(
            "menu_name" => "user"
        );
        $sidebar = $this->load->view('layout/sidebar', $menu, true);
        $footer = $this->load->view('layout/footer', array(), true);
        $data = array(
            "head" => $head,
            "sidebar" => $sidebar,
            "footer" => $footer,
            "content" => $content
        );
        $this->load->view('layout/main', $data);
    }

    public function  add()
    {
        $data = array(
            "title" => $this->config->item('page_title'),
        );
        $content = $this->load->view('user/user_add', $data, true);
        /*โหลด layout */
        $head = $this->load->view('layout/head', array(), true);
        $menu = array(
            "menu_name" => "user"
        );
        $sidebar = $this->load->view('layout/sidebar', $menu, true);
        $footer = $this->load->view('layout/footer', array(), true);
        $data = array(
            "head" => $head,
            "sidebar" => $sidebar,
            "footer" => $footer,
            "content" => $content
        );
        $this->load->view('layout/main', $data);


    }

    public function  edit($u_id = NULL)
    {
        if ($u_id == NULL) {
            $u_id = $this->uri->segment(3);
        }
        // Load view
        // Get department info by ID


        $data = array(
            "title" => $this->config->item('page_title'),
            "result" => $data['query'] = $this->user_model->get_user($u_id)
        );
        //  alert($data);


        $content = $this->load->view('user/user_edit', $data, true);
        /*โหลด layout */


        $head = $this->load->view('layout/head', array(), true);
        $menu = array(
            "menu_name" => "user"
        );
        $sidebar = $this->load->view('layout/sidebar', $menu, true);
        $footer = $this->load->view('layout/footer', array(), true);
        $data = array(
            "head" => $head,
            "sidebar" => $sidebar,
            "footer" => $footer,
            "content" => $content
        );
        $this->load->view('layout/main', $data);
    }


    public function  delete($u_id = NULL)
    {
        if ($u_id == NULL) {
            $u_id = $this->uri->segment(3);
        }
        //    alert($u_id);
        $id = $u_id;
        //เรียกใช้ฟังก์ชั่น ใน Model
        $result = $this->user_model->delete_user($id);
        if ($result == 1) {
            echo '<script>alert("ลบข้อมูลเรียบร้อยแล้ว!");</script>';
            redirect('user', 'refresh');
        } else {
            $this->session->set_flashdata("message", "Record Not Updated!");
            redirect('user', 'refresh');
        }
        redirect('user', 'refresh');

    }


    public function save()
    {
        //โหลด Model
        if ($this->input->post('u_id') == null) {
            $data = array(
                'u_name' => $this->input->post('name'),
                'u_email' => $this->input->post('email'),
                'u_pass' => $this->input->post('password'),
                'u_role' => $this->input->post('role'),
                'u_status' => $this->input->post('status')
            );


            //เรียกใช้ฟังก์ชั่น ใน Model
            $result = $this->user_model->insert_user($data);
            if ($result == 1) {
                echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว!");</script>';
                redirect('user', 'refresh');
            } else {
                $this->session->set_flashdata("message", "Record Not Updated!");
                redirect('user', 'refresh');
            }

        } else {
            $id = $this->input->post('u_id');
            $data = array(
                'u_name' => $this->input->post('name'),
                'u_email' => $this->input->post('email'),
                'u_pass' => $this->input->post('password'),
                'u_role' => $this->input->post('role'),
                'u_status' => $this->input->post('status')
            );

            //เรียกใช้ฟังก์ชั่น ใน Model
            $result = $this->user_model->update_user($data, $id);
            if ($result == 1) {
                echo '<script>alert("บันทึกข้อมูลเรียบร้อยแล้ว!");</script>';
                redirect('user', 'refresh');
            } else {
                $this->session->set_flashdata("message", "Record Not Updated!");
                redirect('user', 'refresh');
            }
            redirect('user', 'refresh');
        }


    }


}
