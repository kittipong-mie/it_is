<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disburse_history extends CI_Controller
{
    private $disburse;

    function __construct()
    {
        parent::__construct();
        if (($this->session->userdata('logged_in')) && (((($this->session->userdata('u_role') == '1') OR ($this->session->userdata('u_role') == '2')) && ($this->session->userdata('u_status') == '1')))) {
            //alert($this->session->userdata('logged_in'));
            $this->load->helper("url");
            $this->load->model("equipment_model");
            $this->load->model("type_equipment_model");
            $this->load->model("office_model");
            $this->load->model("disburse_history_model");
            $this->load->database();
            $this->load->library("pagination");
            $this->disburse = new Disburse_history_model();
        } else {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }

    public function index()
    {
        //pagination set
        $config = array();
        $config["base_url"] = base_url() . "disburse_history/index";
        $config["total_rows"] = $this->disburse_history_model->record_count_disburse_history();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        //config bootstrap pagination
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><span>';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_link'] = '&laquo;';
        $config['prev_link'] = '&lsaquo;';
        $config['last_link'] = '&raquo;';
        $config['next_link'] = '&rsaquo;';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

//-------------------------------------------------------//

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data = array(
            "title" => $this->config->item('page_title'),
            "name" => $this->config->item('name'),
            "results" => $this->disburse_history_model->fetch_disburse_history($config["per_page"], $page),
            "links" => $this->pagination->create_links()
        );

//--------------------------------------------------------//

        $content = $this->load->view('disburse/disburse_list', $data, true);
        /*โหลด layout */
        $head = $this->load->view('layout/head', array(), true);
        $menu = array(
            "menu_name" => "disburse_history"
        );
        $sidebar = $this->load->view('layout/sidebar', $menu, true);
        $footer = $this->load->view('layout/footer', array(), true);
        $data = array(
            "head" => $head,
            "sidebar" => $sidebar,
            "footer" => $footer,
            "content" => $content
        );
        $this->load->view('layout/main', $data);

    }


    public function  add()
    {
        $data = array(
            "title" => $this->config->item('page_title'),
            "name" => $this->config->item('name'),
            "results" => $this->type_equipment_model->get_type_equipment_list(),
            "results2" => $this->office_model->get_office_list(),
            "types" => $this->disburse_history_model->get_type()
        );


        $content = $this->load->view('disburse/disburse_add', $data, true);
        /*โหลด layout */
        $head = $this->load->view('layout/head', array(), true);
        $menu = array(
            "menu_name" => "disburse_history"
        );
        $sidebar = $this->load->view('layout/sidebar', $menu, true);
        $footer = $this->load->view('layout/footer', array(), true);
        $data = array(
            "head" => $head,
            "sidebar" => $sidebar,
            "footer" => $footer,
            "content" => $content
        );
        $this->load->view('layout/main', $data);
    }

    public function  get_equipment($type_id = false)
    {

        $datas = ($this->disburse->get_equipment_from_type($type_id));
        $data = array(
            "result" => $datas
        );
        $this->load->view('disburse/select', $data);
    }

    public function  get_equipment_amount_unit($eq_id = false)
    {
        $datas = ($this->disburse->get_equipment($eq_id));
        $data = array(
            "result" => $datas
        );
        // alert($data);
        $this->load->view('disburse/amount_unit', $data);
    }

    public function  delete($dh_id = null)
    {
        if ($dh_id == NULL) {
            $dh_id = $this->uri->segment(3);

        }
        //ถ้า id ว่างให้เท่ากับค่าใน url segment ที่3
        //นำid ไปหาข้อมูล ของการเบิก
        $id = $dh_id;
        //เรียกใช้ฟังก์ชั่น ใน Model การลบ
        $results = $this->disburse_history_model->get_disburse($id);
        //alert($results);
        //--------------------------------------------
        //ได้ข้อมูลอุปกรณ์และจำนวนที่ลบไป
        foreach ($results as $data) {
            $id = $data['eq_id'];
            $del_eq_amount = $data['eq_amount'];
        }
     //------------------------------------------------
        //นำeq_id ไปหาจำนวนหลักที่มีอยู่ เพื่ออัพเดทข้อมูลจำนวน
        $result_eq_amount = $this->equipment_model->get_equipment($id);
               foreach ($result_eq_amount as $data) {
                $old_amount = $data['eq_amount'];
                 }
        //จำนวนวที่ลบไป บวกกับ  นำจำนวนเดิม
        $newamount = ($del_eq_amount + $old_amount);
        //ตั้งค่าข้อมูลเพื่อส่งให้กับ update_model
        $datas = array(
            'eq_amount' => $newamount
        );

        $result_update = $this->equipment_model->update_equipment_amount($datas, $id);
        $id=$dh_id;
        $result = $this->disburse_history_model->delete_disburse_history($id);
//        alert($result_update);
//        alert($result);
//        die;
        //----------------------------------------------------------------------
        //check data มีการลบและการupdate หรือไม่
        if (($result == 1) && ($result_update == 1)) {
            echo '<script>alert("ลบข้อมูลเรียบร้อยแล้ว!");</script>';
            redirect('disburse_history', 'refresh');
        } else {
            $this->session->set_flashdata("message", "Record Not Updated!");
            redirect('disburse_history', 'refresh');
        }
        redirect('disburse_history', 'refresh');
    }



    public function save()
    {
        if (($this->input->post('amount')) <= ($this->input->post('eq_amount'))) {
            //  alert($_POST);
            $data = array(
                'u_id' => $this->input->post('u_id'),
                'eq_id' => $this->input->post('equipment'),
                'eq_amount' => $this->input->post('amount'),
                'dh_name' => $this->input->post('dh_name'),
                'off_id' => $this->input->post('office'),
                'dh_lender_name' => $this->input->post('dh_lender_name')

            );

            $datas = array(
                'eq_amount' => ($this->input->post('eq_amount') - $this->input->post('amount'))
            );

            $id = $this->input->post('equipment');
            // alert($data);
            // alert($datas);
            // alert($new_amount);
            //alert($id);


            $result = $this->disburse_history_model->insert_disburse_history($data);
            $results = $this->equipment_model->update_equipment_amount($datas, $id);

            // alert($result);
            // alert($results);
            //die;

            if (($result == 1) && ($results == 1)) {
                echo '<script>alert("บันทึกเรียบร้อยแล้ว!");</script>';
                redirect('disburse_history', 'refresh');
            } else {
                $this->session->set_flashdata("message", "Record Not Updated!");
                redirect('disburse_history', 'refresh');
            }
//-------------------------------------------------------------------------------------
        } else {
            echo '<script>alert("ไม่สามารถ เบิกเกินยอดคงเหลือได้ กรุณาระบุจำนวนใหม่อีกครั้ง");</script>';
            redirect('disburse_history/add', 'refresh');
        }


    }


}
